1. Установка Docker и Docker Compose

    sudo apt update <br />
    sudo apt install -y docker.io

    Добавление текущего пользователя в группу docker (чтобы не использовать sudo для команд Docker) <br />
    sudo usermod -aG docker $alex <br />
    newgrp docker

    Установка Docker Compose <br />
    sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose <br />
    sudo chmod +x /usr/local/bin/docker-compose

    Проверка установки <br />
    docker --version <br />
    docker-compose --version

2. Установка Ansible

    sudo apt update <br />
    sudo apt install -y ansible

    Проверка установки <br />
    ansible --version

3. Установка Git

    sudo apt update <br />
    sudo apt install -y git

    Проверка установки <br />
    git --version

Создание проекта

1. Создание рабочей директории для проекта

    mkdir devops_project <br />
    cd devops_project

2. Создание структуры каталогов и файлов

    mkdir openresty <br />
    touch openresty/Dockerfile openresty/nginx.conf docker-compose.yml deploy.yml hosts .gitlab-ci.yml

3. Заполняем файлы:

Dockerfile - отвечает за то как будет построен Docker-образ OpenResty <br />
nginx.conf - отвечает за то как сервер OpenResty будет проксировать запросы на Netdata <br />
docker-compose.yml - описывает и управляет многоконтейнерными Docker приложениями <br />
hosts - указываем на каком сервере будут выполняться действия playbook'а <br />
deploy.yml - автоматизирует процесс развертывания Docker-контейнеров <br />
.gitlab-ci.yml - определение этапов сборки и развертывания

3.1. Dockerfile

    # Dockerfile for OpenResty
    FROM debian:bullseye

    # Install necessary packages and OpenResty
    RUN apt-get update && \
        apt-get install -y curl gnupg2 && \
        curl -fsSL https://openresty.org/package/pubkey.gpg | apt-key add - && \
        echo "deb http://openresty.org/package/debian bullseye openresty" | tee /etc/apt/sources.list.d/openresty.list && \
        apt-get update && \
        apt-get install -y openresty=1.21.4.3-2~bullseye1

    # Expose port 8080
    EXPOSE 8080

    # Copy OpenResty configuration
    COPY nginx.conf /usr/local/openresty/nginx/conf/nginx.conf

    # Start OpenResty
    CMD ["openresty", "-g", "daemon off;"]

3.2. nginx.conf

    worker_processes  1;

    events {
        worker_connections  1024;
    }

    http {
        include       mime.types;
        default_type  application/octet-stream;

        sendfile        on;
        keepalive_timeout  65;

        server {
            listen       8080;
            server_name  localhost;

            location / {
                proxy_pass http://netdata:29999;
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto $scheme;
            }
        }
    }

3.3. docker-compose.yml

    version: '3'

    services:
    openresty:
        build: ./openresty
        ports:
        - "8080:8080"
        depends_on:
        - netdata

    netdata:
        image: netdata/netdata:v1.22.1
        ports:
        - "29999:19999"
        environment:
        - PGID=0
        - PUID=0
        cap_add:
        - SYS_PTRACE
        security_opt:
        - apparmor=unconfined

3.4. hosts

    [servers]
    localhost ansible_connection=local

3.5. deploy.yml

- hosts: servers
  tasks:
    - name: Ensure Docker is installed
      apt:
        name: docker.io
        state: present
        update_cache: yes

    - name: Ensure Docker Compose is installed
      get_url:
        url: https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)
        dest: /usr/local/bin/docker-compose
        mode: '0755'

    - name: Create project directory
      file:
        path: /opt/devops_project
        state: directory

    - name: Copy project files
      copy:
        src: .
        dest: /opt/devops_project
        exclude: "*.git"
    - name: Build and start containers
      command: docker-compose up -d
      args:
        chdir: /opt/devops_project

3.6. .gitlab-ci.yml

    stages:
    - build
    - deploy

    build:
    stage: build
    script:
        - echo "Building Docker images"
        - docker-compose build

    deploy:
    stage: deploy
    script:
        - echo "Deploying application"
        - ansible-playbook deploy.yml
    only:
        - main
